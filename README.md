# Outils pour hts tvheadend


Quelques outils pour la configuraton et l'utilisation de hts tvheadend.

Ces outils ont été testé sur hts tvheadend 4.0 et développé sous
GNU/Linux. Ils devraient fonctionner sous windows et OSX (à condition
d'avoir installé en environemment python 2.X).


## Outils


### FreeBoxTV importer

Importe les IPTVs free.fr sous forme de muxes dans hts.

Exemple d'utilisation (voir --help pour plus d'options)

```
$ #Créer un network adapté
$ ./freeboxtv_importer.py --hts-network-create

$ #Lister les networks
$ ./freeboxtv_importer.py --hts-networks-list
bef9c65f200b2e3d0c1789eab3968124 (TNTnova)
3a400ad0a3c93d03e6050e7824a12532 (FreeBoxTV)

$ #Importer les chaines
$ ./freeboxtv_importer.py --hts-network-uuid 3a400ad0a3c93d03e6050e7824a12532
```


### FreeBoxTv assign number

Assigne les numéros de chaînes "free" à vos chaines hts.

Exemple d'utilisation (voir --help pour plus d'options)

```
$ ./freeboxtv_assign_number.py
```


### Automatics xmltvids

Associe automatiquement les xmltvids (à lancer une fois xmltv configuré).

Exemple d'utilisation (voir --help pour plus d'options)

```
$ ./automatics_xmltvids.py
```


### Autoremove older (records)

Supprime les anciens enregistrements pour concerver un espace disque
donné.

Exemple d'utilisation (voir --help pour plus d'options)

```
$ ./autoremove_older.py --records-dir PATH
```


### tv_grab_fr_mafreebox_hts

Récupère le guide sur la freebox au format xmltv. Il est possible de
récupérer la liste des chaînes configurée sur hts mais il est égallement
possible de ce servir de ce grabbeur sans hts.

  - [Basé sur le travail de tr4ck3ur](https://mythtv-fr.org/forums/viewtopic.php?pid=25265#p25265)
  - [Topic dédié](https://mythtv-fr.org/forums/viewtopic.php?pid=25270#p25270)

Exemple d'utilisation (voir --help pour plus d'options)

```
$ sudo -u hts ./tv_grab_fr_mafreebox_hts.py --configure
$ sudo -u hts ./tv_grab_fr_mafreebox_hts.py
```

Si  tv_grab_fr_mafreebox_hts n'apparait pas dans hts en moyen de contourné
le problème est de configurer tv_grab_combiner

```
$ sudo -u hts tv_grab_combiner --configure
```


## Support

Gestionnaire de ticket gitlab ou irc (SnouF sur freenode, régulièrement
sur [#mythtv-fr](http://mythtv-fr.org/forums/chat.php))
